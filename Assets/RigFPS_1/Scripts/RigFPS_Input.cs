using UnityEngine;

namespace N_RigFPS
{
	public class RigFPS_Input : MonoBehaviour
	{
		public float Vertical { get; private set; }
		public float Horizontal { get; private set; }
		
		private void FixedUpdate()
		{
			Vertical = Input.GetAxis("Horizontal");
			Horizontal = Input.GetAxis("Vertical");
		}
	}
}