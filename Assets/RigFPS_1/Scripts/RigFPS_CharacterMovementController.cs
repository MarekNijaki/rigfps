using UnityEngine;

namespace N_RigFPS
{
	public class RigFPS_CharacterMovementController : MonoBehaviour
	{
		/// <summary>
		///   Flag if movement controller should take character forward vector or camera forward vector as direction orientation. 
		/// </summary>
		[Tooltip("Flag if movement controller should take character forward vector or camera forward vector as direction orientation")]
		public bool _isDirectionRelativeToCharacter;
		public float _turnSpeed = 5.0F;
		public KeyCode _sprintJoystick = KeyCode.JoystickButton2;
		public KeyCode _sprintKeyboard = KeyCode.LeftShift;

		[SerializeField]
		[Tooltip("Animator associated with character")]
		private Animator _anim;

		private RigFPS_Input _rigFPSInput;
		private Camera _mainCamera;
		private float _turnSpeedMultiplier;
		private float _isMovingForwardOrBackward;
		private Quaternion _rotation;
		private bool _isSprinting;
		private float _speed;
		private Vector3 _direction;
		private float _tmpVelocity;
		
		private static readonly int _animPropertyNameForSpeed = Animator.StringToHash("Speed");
		private static readonly int _animPropertyNameForIsMovingForwardOrBackward = Animator.StringToHash("IsMovingForwardOrBackward");
		private static readonly int _animPropertyNameForIsSprinting = Animator.StringToHash("IsSprinting");
		private const float _animSpeedSmoothTime = 0.1F;

		/// <summary>
		///   Start.
		/// </summary>
		private void Start()
		{
			if(_anim == null)
			{
				_anim = GetComponent<Animator>();
			}
			_mainCamera = Camera.main;
			_rigFPSInput = FindObjectOfType<RigFPS_Input>();
		}

		/// <summary>
		///   Update is called once per frame.
		/// </summary>
		private void FixedUpdate()
		{
			DetermineCharacterSpeed();
			SetAnimPropertyForSpeed();

			DetermineIfCharacterIsMovingForwardOrBackward();
			SetAnimPropertyForIsMovingForwardOrBackward();

			DetermineIfCharacterIsSprinting();
			SetAnimPropertyForIsSprinting();

			DetermineDirection();
			ApplyRotationToTransform();
		}

		/// <summary>
		///   Determine/compute character speed.
		/// </summary>
		private void DetermineCharacterSpeed()
		{
			// Set speed of character, based on vertical and horizontal inputs.
			if(_isDirectionRelativeToCharacter)
			{
				_speed = Mathf.Abs(_rigFPSInput.Vertical) + _rigFPSInput.Horizontal;
			}
			else
			{
				_speed = Mathf.Abs(_rigFPSInput.Vertical) + Mathf.Abs(_rigFPSInput.Horizontal);
			}
			// Clamp speed.
			_speed = Mathf.Clamp(_speed, 0f, 1f);
			// Smooth speed of character based on last speed of animation. 
			// This will make animation less jagged.
			_speed = Mathf.SmoothDamp(_anim.GetFloat(_animPropertyNameForSpeed), _speed, ref _tmpVelocity, smoothTime: _animSpeedSmoothTime);
		}
		
		/// <summary>
		///   Set 'Speed' property for animation.
		/// </summary>
		private void SetAnimPropertyForSpeed()
		{
			_anim.SetFloat(_animPropertyNameForSpeed, _speed);
		}

		/// <summary>
		///   Determine/compute if character is moving forward or backward.
		/// </summary>
		private void DetermineIfCharacterIsMovingForwardOrBackward()
		{
			if((_rigFPSInput.Horizontal < 0.0F) && (_isDirectionRelativeToCharacter))
			{
				// For movement relative to character, player can move backwards.
				_isMovingForwardOrBackward = _rigFPSInput.Horizontal;
			}
			else
			{
				// For movement relative to camera, player cannot move backwards.
				_isMovingForwardOrBackward = 0.0F;
			}
		}

		/// <summary>
		///   Set 'IsMovingForwardOrBackward' property for animation.
		/// </summary>
		private void SetAnimPropertyForIsMovingForwardOrBackward()
		{
			_anim.SetFloat(_animPropertyNameForIsMovingForwardOrBackward, _isMovingForwardOrBackward);
		}

		/// <summary>
		///   Determine/compute if character is sprinting.
		/// </summary>
		private void DetermineIfCharacterIsSprinting()
		{
			_isSprinting = ((Input.GetKey(_sprintJoystick) || Input.GetKey(_sprintKeyboard)) && IsInputEmitting());
		}

		/// <summary>
		///   Set 'IsSprinting' property for animation.
		/// </summary>
		private void SetAnimPropertyForIsSprinting()
		{
			_anim.SetBool(_animPropertyNameForIsSprinting, _isSprinting);
		}
		
		/// <summary>
		///   Determine/compute character movement direction.
		/// </summary>
		private void DetermineDirection()
		{
			if(_isDirectionRelativeToCharacter)
			{
				DetermineDirectionRelativeToCharacter();
			}
			else
			{
				DetermineDirectionRelativeToCamera();
			}
		}

		/// <summary>
		///   Determine/compute movement direction relative to character forward position.
		/// </summary>
		private void DetermineDirectionRelativeToCharacter()
		{
			_turnSpeedMultiplier = 0.2F;
			
			// Get the forward direction of the reference transform.
			Vector3 forward = transform.TransformDirection(Vector3.forward);
			forward.y = 0;

			// Get the right-facing direction of the reference transform.
			Vector3 right = transform.TransformDirection(Vector3.right);

			// determine the direction the player will face based on input and the referenceTransform's right and forward directions
			_direction = (_rigFPSInput.Vertical * right) + (Mathf.Abs(_rigFPSInput.Horizontal) * forward);
		}

		/// <summary>
		///   Determine/compute movement direction relative to camera forward position.
		/// </summary>
		private void DetermineDirectionRelativeToCamera()
		{
			_turnSpeedMultiplier = 1.0F;
			
			// Get the forward direction of the reference transform.
			Vector3 forward = _mainCamera.transform.TransformDirection(Vector3.forward);
			forward.y = 0;

			// Get the right-facing direction of the reference transform.
			Vector3 right = _mainCamera.transform.TransformDirection(Vector3.right);

			// Determine the direction.
			_direction = (_rigFPSInput.Vertical * right) + (_rigFPSInput.Horizontal * forward);
		}

		/// <summary>
		///   Check if input is emitting values. 
		/// </summary>
		/// <returns>Flag if input is emitting values</returns>
		private bool IsInputEmitting()
		{
			return ((_rigFPSInput.Vertical != 0.0F) || (_rigFPSInput.Horizontal != 0.0F));
		}
		
		/// <summary>
		///   Apply rotation to transform (rotation is not based on animation).
		/// </summary>
		private void ApplyRotationToTransform()
		{
			if(!IsInputEmitting())
			{
				return;
			}
			
			// ???
			if(_direction.magnitude <= _animSpeedSmoothTime)
			{
				return;
			}
			
			// Calculate rotation.
			_rotation = Quaternion.LookRotation(_direction.normalized, transform.up);
			// If there is rotation.
			if(_rotation.eulerAngles.y - transform.eulerAngles.y != 0)
			{
				Vector3 euler = new Vector3(0, _rotation.eulerAngles.y, 0);
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(euler), _turnSpeed * _turnSpeedMultiplier * Time.deltaTime);
			}
		}
	}
}