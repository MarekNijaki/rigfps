using UnityEngine;

namespace N_RigFPS
{
	public class RigFPS_2_CharacterMovementController : MonoBehaviour
	{
		#region Serialised fields
		
		[SerializeField]
		[Tooltip("Animator associated with character")]
		private Animator _anim;
		
		#endregion
		
		#region Private fields
		
		/// <summary>
		///   Main camera.
		/// </summary>
		private Camera _mainCamera;
		
		/// <summary>
		///   Input.
		/// </summary>
		private RigFPS_Input _rigFPSInput;
		
		/// <summary>
		///   Animator property name for 'InputX'.
		/// </summary>
		private static readonly int _animPropertyNameForInputX = Animator.StringToHash("InputX");
		
		/// <summary>
		///   Animator property name for 'InputY'.
		/// </summary>
		private static readonly int _animPropertyNameForInputY = Animator.StringToHash("InputY");

		#endregion
		
		
		/// <summary>
		///   Start.
		/// </summary>
		private void Start()
		{
			if(_anim == null)
			{
				_anim = GetComponent<Animator>();
			}
			_mainCamera = Camera.main;
			_rigFPSInput = FindObjectOfType<RigFPS_Input>();
		}

		/// <summary>
		///   Update is called once per frame.
		/// </summary>
		private void Update()
		{
			_anim.SetFloat(_animPropertyNameForInputX, _rigFPSInput.Vertical);
			_anim.SetFloat(_animPropertyNameForInputY, _rigFPSInput.Horizontal);
		}
	}
}